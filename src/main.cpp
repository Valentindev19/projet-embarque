// Imports
#include "DHT.h"
#include <Adafruit_Sensor.h>
// Pin sur laquelle le DHT22 est connecté
#define DHTTYPE DHT22 // DHT utilisé
#define DHT22_PIN 13

DHT dht(DHT22_PIN, DHTTYPE);

int a = 2;
int b = 3;
int c = 4;
int d = 5;
int e = 6;
int f = 7;
int g = 8;

// Configuration des blocs
int GND1 = 9;
int GND2 = 10;
int GND3 = 11;
int GND4 = 12;

// Digits
int dig1 = 0;
int dig2 = 0;
int dig3 = 0;
int dig4 = 0;

// Mesures
int Temp;
int Humi;

// Compteurs
int i;
int j;

void zero()
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, HIGH);
}

void one()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void two()
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, LOW);
}

void three()
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, LOW);
}

void four()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void five()
{
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void six()
{
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void seven()
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, HIGH);
  digitalWrite(g, HIGH);
}

void eight()
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void nine()
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}
void o()
{
  digitalWrite(a, LOW);
  digitalWrite(b, LOW);
  digitalWrite(c, HIGH);
  digitalWrite(d, HIGH);
  digitalWrite(e, HIGH);
  digitalWrite(f, LOW);
  digitalWrite(g, LOW);
}

void C()
{
  digitalWrite(a, LOW);
  digitalWrite(b, HIGH);
  digitalWrite(c, HIGH);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, LOW);
  digitalWrite(g, HIGH);
}

void oo()
{
  digitalWrite(a, HIGH);
  digitalWrite(b, HIGH);
  digitalWrite(c, LOW);
  digitalWrite(d, LOW);
  digitalWrite(e, LOW);
  digitalWrite(f, HIGH);
  digitalWrite(g, LOW);
}

void DisplayNumber(int x)
{
  switch (x)
  {
  case 0:
    zero();
    break;
  case 1:
    one();
    break;
  case 2:
    two();
    break;
  case 3:
    three();
    break;
  case 4:
    four();
    break;
  case 5:
    five();
    break;
  case 6:
    six();
    break;
  case 7:
    seven();
    break;
  case 8:
    eight();
    break;
  case 9:
    nine();
    break;
  case 10:
    o();
    break;
  case 11:
    C();
    break;
  case 12:
    oo();
    break;
  }
}

// Mettre à OFF les LEDS
void putLedsOff() {
  digitalWrite(A1, LOW);
  digitalWrite(A2, LOW);
}

// Configuration au démarrage
void setup()
{
  Serial.begin(9600);
  dht.begin();
  // Config PINS
  pinMode(a, OUTPUT);
  pinMode(b, OUTPUT);
  pinMode(c, OUTPUT);
  pinMode(d, OUTPUT);
  pinMode(e, OUTPUT);
  pinMode(f, OUTPUT);
  pinMode(g, OUTPUT);
  pinMode(GND1, OUTPUT);
  pinMode(GND2, OUTPUT);
  pinMode(GND3, OUTPUT);
  pinMode(GND4, OUTPUT);

  // Config LEDS
  pinMode(A1, OUTPUT);
  digitalWrite(A1, LOW);
  pinMode(A2, OUTPUT);
  digitalWrite(A2, LOW);
}

// Boucle appelée à l'infini après l'exécution de setup()
void loop()
{
  // Lecture de la température
  Temp = dht.readTemperature();

  // Allumage de la LED verte si la température est entre 17 et 24 degrés ; la rouge sinon
  (Temp >= 17 && Temp <= 24) ? digitalWrite(A1, HIGH) : digitalWrite(A2, HIGH);
  Serial.print("Température : ");
  Serial.print(Temp);
  Serial.print(" deg. C");
  Serial.print("\n");
  
  for (int i = 1; i <= 250; i++)
  {
    dig1 = Temp / 10;
    dig2 = Temp - (dig1 * 10);
    dig3 = 10; // Degré
    dig4 = 11; // C
    digitalWrite(GND4, HIGH); //digit 4
    DisplayNumber(dig4);
    delay(4);
    digitalWrite(GND4, LOW);
    digitalWrite(GND3, HIGH); //digit 3
    DisplayNumber(dig3);
    delay(4);
    digitalWrite(GND3, LOW);
    digitalWrite(GND2, HIGH); //digit 2
    DisplayNumber(dig2);
    delay(4);
    digitalWrite(GND2, LOW);
    digitalWrite(GND1, HIGH); //digit 1
    DisplayNumber(dig1);
    delay(4);
    digitalWrite(GND1, LOW);
  }

  putLedsOff();

  // Lecture de l'humidité
  Humi = dht.readHumidity();

  // Allumage de la LED verte si l'humidité est entre 40% et 60% ; la rouge sinon
  (Humi >= 40 && Humi <= 60) ? digitalWrite(A1, HIGH) : digitalWrite(A2, HIGH);

  Serial.print("Humidité : ");
  Serial.print(Humi);
  Serial.print("%");
  Serial.print("\n");
  for (int j = 1; j <= 250; j++)
  {
    dig1 = Humi / 10;
    dig2 = Humi - (dig1 * 10);
    dig3 = 10; // Pourcentage (o en haut)
    dig4 = 12; // Pourcentage (o en bas)
    digitalWrite(GND4, HIGH); //digit 4
    DisplayNumber(dig4);
    delay(4);
    digitalWrite(GND4, LOW);
    digitalWrite(GND3, HIGH); //digit 3
    DisplayNumber(dig3);
    delay(4);
    digitalWrite(GND3, LOW);
    digitalWrite(GND2, HIGH); //digit 2
    DisplayNumber(dig2);
    delay(4);
    digitalWrite(GND2, LOW);
    digitalWrite(GND1, HIGH); //digit 1
    DisplayNumber(dig1);
    delay(4);
    digitalWrite(GND1, LOW);
  }

  putLedsOff();
}