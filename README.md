# Comment exploiter Arduino pour notre bien-être ?
## Contexte
Tout d'abord nous allons commencer par introduire le sujet, pourquoi l'avoir choisi, et expliquer le choix des différents outils et différents équipements.

Le projet que nous avons imaginé est l'exploitation d'Arduino pour notre santé : Comment mettre à disposition, un boîtier Arduino, pour pouvoir être dans des conditions favorables ?
Nous avons choisi d'utiliser des capteurs de température et de taux d'humidité afin de pouvoir mesurer les données qu'il y aurait dans une salle puis de les comparer avec les valeurs moyennes qui sont bonnes pour la santé et avertir l'utilisateur. C'est le défi que nous avons décidé de relever avec notre groupe composé de ET-TALEBY Salah-Eddine, DEVAUD Valentin et OMARI Elias.

Nous allons d'abord commencer par expliquer pourquoi avons nous choisi Arduino pour ce travail.
Arduino est un processus simple à comprendre. Il est simple à mettre en place et nous pouvons facilement arriver à quelque chose de complet sans forcément écrire beaucoup de code. Une des raisons est également le fait que ce soit vraiment modulable (nous pouvons ajouter simplement des modules tels qu'un capteur de température ou un capteur d'ensoleillement afin de pouvoir les assembler et arriver à un résultat combinant tout cela.
C'est pour ces raisons que nous avons décidé de prendre Arduino comme support matériel. Ce projet est également intéressant car il nous a permis d'utiliser les compétences que nous avons acquises lors des différents cours et TP et nous avons pu donc les pousser encore pour pouvoir réussir ce projet.
Et finalement, ce projet est un projet intéressant car il nous a permis de découvrir comment se passait un projet Arduino et pouvoir réaliser un sujet sérieux en utilisant des équipements de conception embarquée.

### Circuit voulu
Afin de réaliser ce projet nous avons décidé de réaliser le circuit suivant :
<div align="center"><img src="https://media.discordapp.net/attachments/900664811518566420/958091464385130546/unknown.png" width="800"></div>

## Architecture de la solution
Notre solution est composée d'une carte Arduino Uno, d'un capteur DHT22, d'un afficheur 4-digits 7-segments et de LEDS.
L'ensemble de nos composants sont reliés à la carte Arduino Uno qui est le microcontrôleur de notre solution. 
Il y a deux types de liaisons entre les composants et la carte Arduino : 
- 1-Wire / One-Wire : bus de communication utilisant que un seul port pour extraire/envoyer des données.
- GPIO : liaison la plus souvent utilisée, permettant de communiquer avec d'autres circuits électroniques.

Voici donc le schéma de la solution :
<div align="center"><img src="https://cdn.discordapp.com/attachments/958062200562540574/958062713152602152/unnamed.png?raw=true" width="1000"></div>

## Réalisation
### Matériel utilisé
Pour la réalistion de notre solution, nous avons utilisé plusieurs équipements.
La pièce principale et centrale de notre réalisation est la carte Arduino Uno. C'est un microcontrôleur qui permet de faire fonctionner différents modules en les connectant. 

Notre solution consiste à récupérer la température et le taux d'humidité d'une pièce. Pour cela nous avons besoin d'un module permetant de récuperer ces valeurs.
Le capteur DHT22 permet cela :
<div align="center"><img src="https://cdn.discordapp.com/attachments/958062200562540574/958062480544911461/dht22-31502.jpg?raw=true" width="200"></div>

Nous avons aussi besoin de pouvoir afficher les données en temps réel. Nous savons décidé d'utiliser un afficheur 4-digits 7-segments :
<div align="center"><img src="https://cdn.discordapp.com/attachments/958062200562540574/958062480272289802/affichuer.jpg?raw=true" width="200"></div>

2 LEDS sont utilisées (une rouge et une verte) : elles permettent de signaler le respect ou non des bonnes conditions de bien-être dans la pièce :
<div align="center"><img src="https://media.discordapp.net/attachments/819645719370924072/958087782293717043/unknown.png" width="200"></div>

### Branchements
Le premier branchement utilisé est le branchement entre la carte Arduino Uno et le capteur DHT22, avec une laison 1-Wire :
<div align="center"><img src="https://cdn.discordapp.com/attachments/958062200562540574/958062481350225970/circuit_DHT22_et_Arduino.png?raw=true" width="600"></div>
Le capteur a besoin d'une alimentation 5V et d'être relié à la terre. Un seul port est utilisé pour pouvoir communiquer avec le capteur.

Nous avons dû aussi relier la carte Arduino Uno et l'afficheur 4-digits 7-segments.
<div align="center"><img src="https://cdn.discordapp.com/attachments/958062200562540574/958062482033868830/dht11_schema.jpg?raw=true" width="800"></div>


Ce branchement nécessite l'utilisation de 12 ports. Donc l'ensemble des 12 ports de la carte Arduino disponibles seraient utilisés et il serait impossible de relier le capteur. Pour cela nous avons relié que 11 ports de l'afficheur (nous n'allons pas relié le port 12 de l'afficheur qui correspond au point ".").

Pour finir, nous relions deux LEDS à notre carte.
<div align="center"><img src="https://cdn.discordapp.com/attachments/958062200562540574/958062480066748416/montageLed-1024x732.png?raw=true" width="600"></div>

Nous rajoutons deux résistances entre les LEDS et la carte, ce qui permet de réduire la luminosité des LEDS.

### Outils utilisés
<div align="center"><img src="https://media.discordapp.net/attachments/900664811518566420/958097652799647765/outils.png" width="800"></div>

### Programmation
#### Librairies
Pour développer notre solution, nous avons utilisé les librairies suivantes :
- DHT & Adafruit Sensor : pour communiquer avec le capteur DHT22 et récupérer les données en temps réel, en utilisant le protocole 1-Wire.
- SevSeg (dans un premier temps) : pour afficher les données (température / humidité) sur l'afficheur 4-digits 7-segments. Mais la librairie n'autorisant pas les delay() et rendant le rafraîchissement de l'affichage complexe, nous nous sommes rabattus sur la méthode traditionnelle : avec l'utilisation de la fonction digitalWrite().

#### Configuration des PINs
<div align="center"><img src="https://cdn.discordapp.com/attachments/958062200562540574/958062479840260176/Arduino-Uno-Pin-Diagram.png?raw=true" width="600"></div>
<div align="center"><img src="https://cdn.discordapp.com/attachments/958062200562540574/958062481052401704/digit.png?raw=true" width="400"></div>
Comme expliqué précédemment, nous utilisons 7 ports : 2 jusqu'à 8 (le port 1 et 2 ne sont pas utilisables).
Nous n'utilisons pas le point "." dans notre affichage, un port supplémentaire n'est donc pas obligatoire et cela nous facilite la tâche pour connecter le capteur DHT22.
Ainsi, chaque segment est connecté à un port. Exemple : segment A => port 2 / segment B => port 3...

Et pour configurer les PINs, nous utilisons la fonction pinMode() dans le setup() :
<div align="center"><img src="https://media.discordapp.net/attachments/819645719370924072/958095804801904681/unknown.png" width="200"></div>

#### Affichage d'un caractère
Pour afficher un caractère sur un digit, il faut gérer les 7 segments (les allumer ou les éteindre). 
Et comme expliqué ci-dessus, nous nous gérons pas les nombres décimaux, donc le segment '.' a été désactivé. 

Ainsi, pour afficher le caractère 'C', il faut allumer les digits : A, F, G, C, D.
En utilisant la fonction digitalWrite(), nous pouvons spécifier les segments à allumer/éteindre avec le paramètre LOW/HIGH (LOW correspond à un segment allumé).
<div align="center"><img src="https://cdn.discordapp.com/attachments/958062200562540574/958062480804954242/code_c.png?raw=true" width="200"></div>

## Retour d'expérience
### Contraintes
Les principales contraintes durant ce projet étaient le temps et le matériel disponible.
En effet, nous n'avions qu'une vingtaine d'heures pour créer et développer notre solution. Ceci a limité nos idées de conception : il fallait trouver une solution viable et possible à concevoir en très peu de temps.
De plus, le matériel disponible a également freiné notre créativité : il n'y avait pas de capteur de son, de CO² etc. Imaginer un système complexe et innovant était donc assez contrainant en raison du peu de matériel.
### Problèmes rencontrés
Malgré la réussite du projet, nous avons quand-même recontré quelques problèmes, qui ont été corrigés entre temps :
- L'affichage de plusieurs caracètres en même temps : ceci n'est pas possible. Il faut ajouter des delay() très courts entre l'affichage de chaque caractère (de l'ordre de 4ms car non-visible par l'humain).
- Le manque de ports : nous avons utilisé les ports analogiques pour résoudre ce problème et connecter le capteur DHT22 à la carte Arduino.
### Évolution du projet
Le projet est très modulable et évolutif. Nous aurions aimé pouvoir connecter d'autres capteurs (CO² par-exemple), envoyer les données sur une page-web accessible par l'utilisateur avec des statistiques ou utiliser un autre afficheur beaucoup plus simple et ergonomique.
## Support présentation 
https://docs.google.com/presentation/d/1DVn2jliVAQuqifhyMu24gihgK_cGyLOD3TvrKq7CGpI/edit?usp=sharing

